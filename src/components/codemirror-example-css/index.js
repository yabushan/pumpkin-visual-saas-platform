import Vue from 'vue';
import CodemirrorExampleCss from './index.vue';

const Constructor = Vue.extend(CodemirrorExampleCss);

const show = (option = {}) => {
    const vm = new Constructor()
    vm.$mount()
    document.body.appendChild(vm.$el);
    vm.show(option);
}

const runCss = (styleInfo) => {
    const vm = new Constructor()
    vm.$mount()
    document.body.appendChild(vm.$el);
    vm.runCss(styleInfo);
}
export default {show,runCss};

 
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Designer from '@/views/designer/index'
import Preview from '@/views/preview/index'
import Layout from "@/views/manage/Layout";
import DesignList from "@/views/manage/DesignList";
import ImgPool from "@/views/manage/ImgPool";
import Login from "@/views/manage/Login";
import ExcelFile from "@/views/manage/ExcelFile";
import DataSource from "@/views/manage/Datasource";
import DataApi from "@/views/manage/DataApi";

export const manageMenus = [
    {
        path: '/designList',
        name: 'DesignList',
        component: DesignList,
        meta: { title: '大屏管理', icon: 'el-icon-data-line' }
    }, {
        path: '/excelFile',
        name: 'excelFile',
        component: ExcelFile,
        meta: { title: 'excel数据管理', icon: 'el-icon-data-line' }
    },{
        path: '/dataSource',
        name: 'dataSource',
        component: DataSource,
        meta: { title: '外部数据源管理', icon: 'el-icon-data-line' }
    },{
        path: '/dataApi',
        name: 'dataApi',
        component: DataApi,
        meta: { title: 'API服务', icon: 'el-icon-data-line' }
    },{
        path: '/imgPool',
        name: 'imgPool',
        component: ImgPool,
        meta: { title: '资源库管理', icon: 'el-icon-data-line' }
    },/* {
        path: '/imgGroup',
        name: 'imgGroup',
        component: () => import('@/views/manage/ImgGroup'),
        meta: { title: '资源分组管理', icon: 'el-icon-data-line' }
    },*/ {
        path: 'http://qxsdcloud.com/jmreport/list',
        name: 'jmreport',
        meta: { title: '报表设计', icon: 'el-icon-data-line', type: 'link' }
    },{
        path: 'http://qxsdcloud.com/yunpan/autologin.html',
        name: 'yunpan',
        meta: { title: '我的云盘', icon: 'el-icon-data-line', type: 'link' }
    },
]

// 创建 router 实例，然后传 `routes` 配置
export default new VueRouter({
    //mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
        }, {
            path: '/',
            component: Layout,
            redirect: '/designList',
            children: manageMenus
        }, {
            path: '/design',
            name: 'Designer',
            component: Designer
        }, {
            path: '/preview',
            name: 'preview',
            component: Preview
        }, {
            path: '/view',
            name: 'view',
            component: Preview
        }
    ]
})

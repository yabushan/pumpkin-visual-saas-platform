import request from '@/utils/HttpUtil'
import { baseUrl } from '/env'

export function pageListApi(params) {
  return request({
    url: baseUrl+'/dataFormInfo/pageList',
    method: 'get',
    params
  })
}
export function saveOrUpdateApi(data) {
  return request({
    url: baseUrl+'/dataFormInfo/saveOrUpdate',
    method: 'post',
    data
  })
}
export function delApi(id) {
  return request({
    url: baseUrl+'/dataFormInfo/delete/'+id,
    method: 'delete'
  })
}

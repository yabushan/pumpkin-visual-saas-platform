import request from '@/utils/HttpUtil'
import { baseUrl } from '/env'

export function getMapDataByCode(code) {
    return request({
        url: baseUrl+'/data/ProvicneMapStr?ApiCode=ff5da3bf388a4e02a907b1a531e37825&param=code/'+code,
        method: 'get'
    })
}
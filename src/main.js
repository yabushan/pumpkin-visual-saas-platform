import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as echarts from 'echarts'
import router from './router'
import dataV from '@jiaminghi/data-view'
import '@/permission' // permission control
import {registerOption} from '@/components/registerOption'
//注册echarts地图
import chinaGeoJson from '@/assets/map/china.json'
import CodemirrorExampleCss from '@/components/codemirror-example-css/index.vue'
import codemirrorExampleCss from '@/components/codemirror-example-css/index.js'

echarts.registerMap('china', chinaGeoJson);
// 注册高德地图
import VueAMap  from 'vue-amap';
Vue.use(VueAMap);
// 初始化vue-amap
VueAMap.initAMapApiLoader({
  // 高德的key
  key: 'you key',
  // 插件集合
  plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor','AMap.Geolocation'],
  v: '1.4.4'
     
});

Vue.prototype.$runScript = function(key,option,params) {
  if(Object.prototype.toString.call(option.eventsData) === '[object Array]') {
    option.eventsData.forEach(item => {
      if (item.eventKey === key) {
        let fun = new Function('params','return ' + item.eventValue);
        fun.bind(this)(params);
      }
    });
  }
}

Vue.component('codemirror-example-css', CodemirrorExampleCss)
Vue.prototype.$echarts = echarts
Vue.prototype.$codemirrorExampleCss = codemirrorExampleCss
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(dataV)
registerOption()

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

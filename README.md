# 小南瓜可视化saas平台

#### 介绍
小南瓜可视化大屏内容展示无需进行定制开发只需要在PC端拖拉拽就可以轻松制作报告并投放到数据大屏。
，而且支持自定义维度和指标，非常方便客户进行猜想式、求证式的数据探索。基于拖拉拽的方式可用于业务数据的快速分析，制作仪表盘，也可构建可视化大屏。






设计器的部署教程：

1、获取源代码

2、进入到form_design文件夹，打开cmd窗口进入到该目录，执行cnpm intall 命令安装依赖


3、将工程导入的自己擅长的开发工具，例如：hbuilder或vscode


4、启动工程
执行，npm  run serve 命令；启动前端设计器


5、用预览模式访问设计器（不需要后端）
访问地址：http://localhost/#/design




演示环境：http://www.nangs.vip:3389/
账号：common   密码：common


操作手册：https://docs.qq.com/doc/DY3FkWGFCTXNoRHRk



案例：
![](https://gitee.com/zhang-jin/pumpkin-visualization-platform/raw/master/image.png)
![](https://gitee.com/zhang-jin/pumpkin-visualization-platform/raw/master/image1.png)
![](https://gitee.com/zhang-jin/pumpkin-visualization-platform/raw/master/image2.png)
![](https://gitee.com/zhang-jin/pumpkin-visualization-platform/raw/master/image3.png)
![](https://gitee.com/zhang-jin/pumpkin-visualization-platform/raw/master/image4.png)
![](https://gitee.com/zhang-jin/pumpkin-visualization-platform/raw/master/image5.png)


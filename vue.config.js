module.exports = {
    publicPath: './',
    outputDir: "dist",
    devServer: {
        disableHostCheck: true,
        port: 80,
		host: '0.0.0.0',
        open: true,
        overlay: {
            warnings: false,
            errors: true
        },
        proxy: {
            '/design': {
                target: 'http://127.0.0.1:6882',
               // ws: false,
                changeOrigin: true,
                pathRewrite: {
                    '^/design': ''
                }
            },
            '/fileUrl': {
                target: 'http://127.0.0.1:6882',
                //ws: false,
                changeOrigin: true,
                pathRewrite: {
                    '^/fileUrl': ''
                }
            },
			
        }
    },
}
